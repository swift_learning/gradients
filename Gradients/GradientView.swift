//
//  GradientView.swift
//  Gradients
//
//  Created by iulian david on 7/11/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit

@IBDesignable
class GradientView: UIView {

    @IBInspectable var firstColor: UIColor = .clear {
        didSet {
            updateView()
        }
    }
    @IBInspectable var secondColor: UIColor = .clear {
        didSet {
            updateView()
        }
    }

    override class var layerClass: AnyClass {
        get {
         return CAGradientLayer.self
        }
    }
    
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [ firstColor.cgColor, secondColor.cgColor ]
    }
}
